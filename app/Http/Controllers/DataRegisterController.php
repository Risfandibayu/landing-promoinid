<?php

namespace App\Http\Controllers;

use App\Models\data_register;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;

class DataRegisterController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     */
    public function show(data_register $data_register)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(data_register $data_register)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, data_register $data_register)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(data_register $data_register)
    {
        //
    }


    public function register(Request $request){
        $register =  new data_register();
        $register->nama_depan = $request->firstname;
        $register->nama_belakang = $request->lastname;
        $register->email = $request->email;
        $register->nama_usaha = $request->usaha;
        $register->negara_asal = $request->country; 
        $register->industri = $request->industri;
        $register->hal_diskusi = $request->hal_diskusi;
        $register->save();

        // $notify[] = ['success', 'Register Berhasil!. Anda akan dihubungi oleh tim, Mohon sabar menunggu'];
        // return back()->withNotify($notify);

        $url = "https://wa.me/6281296051188?text=Hallo%20Promoin!!%0ASaya%20tertarik%20untuk%20berdiskusi%20lebih%20lanjut%20mengenai%20produk-produk%20Anda%2C%20bisa%20berdiskusi%20sebentar%3F%0A%0ANama%20%3A%20".$request->firstname."%20".$request->lastname."%2C%0AEmail%20%3A%20".$request->email."%2C%0ANama%20Usaha%20%3A%20".$request->usaha."%2C%0ANegara%20Asal%20%3A%20".$request->country."%2C%0AIndustri%20%3A%20".$request->usaha."%2C%0AHal%20diskusi%20%3A%20".$request->hal_diskusi."%2C";
        return Redirect::to($url);
    }
}
