<?php

namespace App\Http\Controllers;

use App\Models\data_email;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;

class DataEmailController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     */
    public function show(data_email $data_email)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(data_email $data_email)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, data_email $data_email)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(data_email $data_email)
    {
        //
    }

    public function email(Request $request){
        $email = new data_email();
        $email->email = $request->email;
        $email->save();

        $notify[] = ['success', 'Email berhasil di kirim!'];
        // return back()->withNotify($notify);
        $url = "https://wa.me/6281296051188?text=Hallo%20Promoin!!%0ASaya%20tertarik%20untuk%20berdiskusi%20lebih%20lanjut%20mengenai%20produk-produk%20Anda%2C%20bisa%20berdiskusi%20sebentar%3F%0A%0AEmail%20%3A%20".$request->email;
        return Redirect::to($url);
    }
}
