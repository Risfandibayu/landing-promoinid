<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta content="width=device-width, initial-scale=1.0" name="viewport">

    <title>Promoin.id</title>
    <meta content="" name="description">
    <meta content="" name="keywords">

    <!-- Favicons -->
    <link href="{{asset('public')}}/assets/img/logo2.png" rel="icon">
    <link href="{{asset('public')}}/assets/img/logo2.png" rel="apple-touch-icon">

    <!-- Google Fonts -->
    <link
        href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i|Montserrat:300,300i,400,400i,500,500i,600,600i,700,700i|Poppins:300,300i,400,400i,500,500i,600,600i,700,700i"
        rel="stylesheet">

    <!-- Vendor CSS Files -->
    <link href="{{asset('public')}}/assets/vendor/aos/aos.css" rel="stylesheet">
    <link href="{{asset('public')}}/assets/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <link href="{{asset('public')}}/assets/vendor/bootstrap-icons/bootstrap-icons.css" rel="stylesheet">
    <link href="{{asset('public')}}/assets/vendor/boxicons/css/boxicons.min.css" rel="stylesheet">
    <link href="{{asset('public')}}/assets/vendor/glightbox/css/glightbox.min.css" rel="stylesheet">
    <link href="{{asset('public')}}/assets/vendor/remixicon/remixicon.css" rel="stylesheet">
    <link href="{{asset('public')}}/assets/vendor/swiper/swiper-bundle.min.css" rel="stylesheet">

    <!-- Template Main CSS File -->
    <link href="{{asset('public')}}/assets/css/style.css" rel="stylesheet">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/izitoast/1.4.0/css/iziToast.min.css"
        integrity="sha512-O03ntXoVqaGUTAeAmvQ2YSzkCvclZEcPQu1eqloPaHfJ5RuNGiS4l+3duaidD801P50J28EHyonCV06CUlTSag=="
        crossorigin="anonymous" referrerpolicy="no-referrer" />

    <!-- =======================================================
  * Template Name: Bootslander
  * Updated: Jul 27 2023 with Bootstrap v5.3.1
  * Template URL: https://bootstrapmade.com/bootslander-free-bootstrap-landing-page-template/
  * Author: BootstrapMade.com
  * License: https://bootstrapmade.com/license/
  ======================================================== -->
    <style>
        .floating-btn {
            position: fixed;
            bottom: 16px;
            right: 65px;
            z-index: 1000;
        }
    </style>
</head>

<body>
    <a target="_blank"
        href="https://wa.me/6281296051188?text=Hallo%20Promoin!!%0ASaya%20tertarik%20untuk%20berdiskusi%20lebih%20lanjut%20mengenai%20produk-produk%20Anda%2C%20bisa%20berdiskusi%20sebentar%3F"
        class="btn-get-started floating-btn">Talk With Us Now <i class="bi-chat-dots"></i></a>
    <!-- ======= Header ======= -->
    <header id="header" class="fixed-top d-flex align-items-center header-transparent">
        <div class="container d-flex align-items-center justify-content-between">

            <div class="logo">
                <!-- <h1><a href="#"><span>Promoin id</span></a></h1> -->
                <!-- Uncomment below if you prefer to use an image logo -->
                <a href="#"><img src="{{asset('public')}}/assets/img/logo.png" alt="" class=""></a>
            </div>

            <nav id="navbar" class="navbar">
                <ul>
                    <li><a class="nav-link scrollto active" href="#hero">Home</a></li>
                    <li><a class="nav-link scrollto" href="#product">Product</a></li>
                    <li><a class="nav-link scrollto" href="#about">About Us</a></li>
                </ul>
                <i class="bi bi-list mobile-nav-toggle"></i>
            </nav><!-- .navbar -->
            <a class="btn btn-get-started" href="{{route('register')}}">Register <i
                    class="bi-box-arrow-in-right"></i></a>

        </div>
    </header><!-- End Header -->

    <!-- ======= Hero Section ======= -->
    <section id="hero">

        <div class="container">
            <div class="row justify-content-between">
                <div class="col-lg-7 pt-5 pt-lg-0 order-1 order-lg-1 d-flex align-items-center">
                    <div data-aos="zoom-out">
                        <h1>Promosikan Bisnis Anda dan Datangkan Customer Sebanyak-banyaknya</h1>
                        <h2>Kami menawarkan solusi kreatif dan strategi inovatif yang akan membawa bisnis Anda ke
                            tingkat
                            berikutnya. Segera temukan bagaimana layanan kami mempermudah bisnis Anda dalam
                            berkomunikasi dengan
                            pelanggan secara instan.</h2>
                        <div class="text-center text-lg-start row">
                            <div class="col-6 col-md-4">
                                <a href="{{route('register')}}" class="btn-get-started text-center w-100">Get Started<i
                                        class="bi-chevron-right"></i></a>
                            </div>
                            <div class="col-6 col-md-4">
                                <a target="_blank"
                                    href="https://wa.me/6281296051188?text=Hallo%20Promoin!!%0ASaya%20tertarik%20untuk%20berdiskusi%20lebih%20lanjut%20mengenai%20produk-produk%20Anda%2C%20bisa%20berdiskusi%20sebentar%3F"
                                    class="btn-kontak-kami text-center w-100">Kontak Kami!</a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-5 order-2 order-lg-2 hero-img" data-aos="zoom-out" data-aos-delay="300">
                    <img src="{{asset('public')}}/assets/img/header.png" class="img-fluid" alt="">
                </div>
            </div>
        </div>

    </section><!-- End Hero -->

    <main id="main">

        <section id="product" class="pricing bg-grad">
            <div class="container">

                <div class="section-title text-center" data-aos="fade-up">
                    <p style="color: black;">Produk Kami</p>
                </div>

                <div class="row" data-aos="fade-left">

                    <div class="col-lg-4 col-md-12 mb-2 ">
                        <div class="box text-light h-100" style="background-color: #0AA550;" data-aos="zoom-in"
                            data-aos-delay="100">
                            <img src="{{asset('public')}}/assets/img/p1.png" class="img-fluid w-auto p-4" alt="">
                            <h4 style="color: white;">Broadcast Message</h4>
                            <p>
                                Pesan broadcast tentang produk, jasa, atau informasi apapun dan kirim ke semua customer
                                secara otomatis.
                            </p>
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-12 mb-2">
                        <div class="box text-light  h-100" style="background-color: #004AAD;" data-aos="zoom-in"
                            data-aos-delay="300">
                            <img src="{{asset('public')}}/assets/img/p2.png" class="img-fluid w-auto p-4" alt="">
                            <h4 style="color: white;">Ads Management</h4>
                            <p>
                                Real time monitoring management melalui semua saluran mulai dari online hingga offline.
                            </p>
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-12 mb-2">
                        <div class="box text-light h-100 " style="background-color: #0097B2;" data-aos="zoom-in"
                            data-aos-delay="500">
                            <img src="{{asset('public')}}/assets/img/p3.png" class="img-fluid w-auto p-4" alt="">
                            <h4 style="color: white;">eBilling</h4>
                            <p>
                                Transaksi pembayaran secara digital tanpa persyaratan yang sulit. Tinggal dipakai saja!
                            </p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="bg-top mt-5"></div>
            <div class="container  text-light">

                <div class="section-title text-center pt-5" data-aos="fade-up">
                    <p style="color: white;">Broadcast Message</p>
                    <h5>Featured on WhatsApp Business Platform</h5>
                </div>
                <div class="row justify-content-center align-content-center " data-aos="fade-left">

                    <div class="col-lg-4 d-flex order-2 order-md-1 row col-md-12 mb-5 mb-md-0 ">
                        <div class="col-12 top text-md-end text-center">
                            <h2 class="text-decoration-underline fw-bolder t-dark">Business-Initiated</h2>
                            <p class="t-dark" style="font-size: 20px;">Pelaku bisnis memulai percakapan dengan pelanggan
                            </p>
                        </div>
                        <div class="col-12 bottom text-md-end text-center">
                            <h2 style="color: black; font-weight: bold;">Supported</h2>
                            <div class="">
                                <p class="btn-asd">Structured text</p>
                                <p class="btn-asd">Media</p>
                                <p class="btn-asd">Document</p>
                                <p class="btn-asd">Buttons</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4 order-1 order-md-2 col-md-12 text-center">

                        <img src="{{asset('public')}}/assets/img/hp1.png" class="img-fluid w-50 p-4" alt="">

                    </div>
                    <div class="col-lg-4 d-flex order-3 order-md-3 row  col-md-12">
                        <div class="col-12 top text-md-start text-center">
                            <h2 class="text-decoration-underline fw-bolder t-dark">User-Initiated</h2>
                            <p class="t-dark" style="font-size: 20px;">Pelanggan memulai percakapan dengan Anda</p>
                        </div>
                        <div class="col-12 bottom text-md-start text-center">
                            <h2 style="color: black; font-weight: bold;">Supported</h2>
                            <div class="">
                                <p class="btn-asd">Chatbot</p>
                                <p class="btn-asd">CRM System</p>
                                <p class="btn-asd">3rd Party System Integration</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="bg-top2 mt-5"></div>
            <div class="container pt-5 text-light mb-5">
                <div class="row ">

                    <div class="col-lg-6 d-flex col-md-12 mb-5 mb-md-0 ">
                        <div class="top text-center row" data-aos="fade-left" data-aos-delay="100">
                            <div class="col-md-8 mt-5 justify-content-center align-content-center ">
                                <h1 class=" fw-bolder
                ">Broadcast Message</h1>
                                <h2>WhatsApp Business Platform Use Case</h2>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-2 col-md-12 text-center text-dark " data-aos="fade-left" data-aos-delay="200">

                        <img src="{{asset('public')}}/assets/img/hp3.png" class="img-fluid w-auto " alt=""><br>
                        <strong>Push Notifications (BIC)</strong>

                    </div>
                    <div class="col-lg-2 col-md-12 text-center text-dark" data-aos="fade-left" data-aos-delay="300">

                        <img src="{{asset('public')}}/assets/img/hp4.png" class="img-fluid w-auto" alt=""><br>
                        <strong>User-Initated (UIC)</strong>

                    </div>
                    <div class="col-lg-2 col-md-12 text-center text-dark" data-aos="fade-left" data-aos-delay="400">

                        <img src="{{asset('public')}}/assets/img/hp5.png" class="img-fluid w-auto" alt=""><br>
                        <strong>User Interaction with Chatbot (UIC)</strong>
                    </div>
                </div>
            </div>
            <div class="container pt-5 text-light mt-5">
                <div class="row " data-aos="fade-left">
                    <div class="col-lg-6 col-md-12 text-center text-dark mb-5">

                        <img src="{{asset('public')}}/assets/img/hpp.png" class="img-fluid w-50" alt="">

                    </div>
                    <div class="col-lg-6 d-flex col-md-12 mb-5 mb-md-0 ">
                        <div class="top text-dark text-center text-md-start m-auto">
                            <h1 class="fw-bolder">Ads Management
                            </h1>
                            <h2>Real time monitoring management melalui semua saluran mulai dari online hingga offline.
                                Kami menawarkan beberapa fasilitas digital ads seperti: SMS Broadcast, USSD 747, Flash
                                Message, DIgital Ads Banner, FB Ads, TikTok Ads, Google SEM, dan Instagram Ads.</h2>
                        </div>
                    </div>

                </div>
            </div>
            <div class="container pt-5 text-light">
                <div class="row " data-aos="fade-right">

                    <div class="col-lg-6 d-flex col-md-12 mb-5 mb-md-0 ">
                        <div class="top text-dark text-center text-md-end m-auto">
                            <h1 class="fw-bolder">eBiling
                            </h1>
                            <h2>Transaksi pembayaran secara digital tanpa persyaratan yang sulit. Terintegrasi dengan
                                Payment Gateway dan proses sangat seamless.
                            </h2>
                        </div>
                    </div>
                    <div class="col-lg-6 col-md-12 text-center text-dark mb-5">

                        <img src="{{asset('public')}}/assets/img/pc1.png" class="img-fluid w-50" alt="">

                    </div>
                </div>
            </div>
            <div class="container pt-5 text-light">
                <div class="row " data-aos="fade-left">

                    <div class="col-lg-6 d-flex col-md-12 mb-5 mb-md-0 ">
                        <div class="top text-dark text-center m-auto">
                            <h1 style="font-size: 50px;" class="fw-bolder">Tingkatkan bisnis Anda to the next level
                            </h1>
                            <h2>Tingkatkan penjualan Anda, engagement, hasilkan prospek berkualitas di sosial media
                                Anda.
                            </h2>
                        </div>
                    </div>
                    <div class="col-lg-6 col-md-12 text-center text-dark mb-5">

                        <img src="{{asset('public')}}/assets/img/v1.png" class="img-fluid w-75" alt="">

                    </div>
                </div>
            </div>
        </section>


        <section id="about" class="pricing bg-grad">
            <div class="container">

                <div class="section-title text-center justify-content-center row" data-aos="fade-up">
                    <div class="col-12 col-md-8">
                        <p class="text-center t-enas" style="color: black;">Siap untuk Meningkatkan Bisnis Anda?</p>
                        <h5 class="t-asd">Kontak kami segera untuk penawaran terbaik yang diinginkan.</h5>
                        <div class="subs mb-5">
                            <form action="{{route('email.post')}}" method="post">
                                @csrf
                                <input type="email" placeholder="Masukan E-mail Anda" name="email"><input type="submit"
                                    value="Get Started">
                            </form>
                        </div>
                    </div>
                    <div class="col-12">
                        {{-- <iframe
                            src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3966.164287928979!2d106.8422157!3d-6.2420675!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x2e69f3bf5252f3af%3A0x1f27e45b12c0e6d6!2sGraha%20Mustika%20Ratu!5e0!3m2!1sen!2sid!4v1691735411197!5m2!1sen!2sid"
                            width="100%" height="600" style="border:0;" allowfullscreen="" loading="lazy"
                            referrerpolicy="no-referrer-when-downgrade"></iframe> --}}
                        <iframe
                            src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d126907.08080528809!2d106.71967831253322!3d-6.283929465630962!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x2e69f1ec2422b0b3%3A0x39a0d0fe47404d02!2sSouth%20Jakarta%2C%20South%20Jakarta%20City%2C%20Jakarta!5e0!3m2!1sen!2sid!4v1691736186474!5m2!1sen!2sid"
                            width="100%" height="600" style="border:0;" allowfullscreen="" loading="lazy"
                            referrerpolicy="no-referrer-when-downgrade"></iframe>
                    </div>
                </div>

                <div class="row" data-aos="fade-left">


                </div>
            </div>
        </section><!-- End Pricing Section -->



    </main><!-- End #main -->

    <!-- ======= Footer ======= -->
    <footer id="footer" class="bg-grad">
        <div class="footer-top">
            <div class="container text-dark">
                <div class="row justify-content-center">

                    <div class="col-lg-6 col-md-6 justify-content-center text-center col-12 mb-5">
                        <img src="{{asset('public')}}/assets/img/logo.png" class="img-fluid w-50" alt="">
                    </div>

                    <div class="col-lg-2 col-md-6 col-4 footer-links text-dark">
                        <h4>Promoin.id</h4>
                        <ul>
                            <li><a href="#">Tentang Kami</a></li>
                            <li><a href="#">Tim Kami</a></li>
                            <li><a href="#">Features</a></li>
                            <li><a href="#">Career</a></li>
                        </ul>
                    </div>

                    <div class="col-lg-2 col-md-6 col-4 footer-links">
                        <h4>Support</h4>
                        <ul>
                            <li><a href="#">Bantuan</a></li>
                            <li><a href="#">Feedback</a></li>
                            <li><a href="#">FAQ</a></li>
                        </ul>
                    </div>
                    <div class="col-lg-2 col-md-6 col-4 footer-links">
                        <h4>Follow Us</h4>
                        <ul>
                            <li><a href="#">Instagram</a></li>
                            <li><a href="#">Facebook</a></li>
                            <li><a href="#">Tiktok</a></li>
                            <li><a href="#">Youtube</a></li>
                        </ul>
                    </div>



                </div>
            </div>
        </div>

        <div class="container">
            <div class="copyright">
                &copy; Copyright, <strong><span>Promoin.id</span></strong> 2023. All Rights Reserved
            </div>

        </div>
    </footer><!-- End Footer -->

    <a href="#" class="back-to-top d-flex align-items-center justify-content-center"><i
            class="bi bi-arrow-up-short"></i></a>
    <div id="preloader"></div>

    <!-- Vendor JS Files -->
    <script src="{{asset('public')}}/assets/vendor/purecounter/purecounter_vanilla.js"></script>
    <script src="{{asset('public')}}/assets/vendor/aos/aos.js"></script>
    <script src="{{asset('public')}}/assets/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
    <script src="{{asset('public')}}/assets/vendor/glightbox/js/glightbox.min.js"></script>
    <script src="{{asset('public')}}/assets/vendor/swiper/swiper-bundle.min.js"></script>
    <script src="{{asset('public')}}/assets/vendor/php-email-form/validate.js"></script>

    <!-- Template Main JS File -->
    <script src="{{asset('public')}}/assets/js/main.js"></script>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/izitoast/1.4.0/js/iziToast.min.js"
        integrity="sha512-Zq9o+E00xhhR/7vJ49mxFNJ0KQw1E1TMWkPTxrWcnpfEFDEXgUiwJHIKit93EW/XxE31HSI5GEOW06G6BF1AtA=="
        crossorigin="anonymous" referrerpolicy="no-referrer"></script>

    @if(session()->has('notify'))
    @foreach(session('notify') as $msg)
    <script>
        'use strict';
            iziToast.{{ $msg[0] }}({message:"{{ $msg[1] }}", position: "topRight"});
    </script>
    @endforeach
    @endif

</body>

</html>